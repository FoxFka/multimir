(function( $ ){

    var methods = {
        init: function( options ) {
            var names = [];
            var self = this;
            if (this.length) {
                this.each(function () {
                    names.push($(this).data('modal'));
                });
            }
            if (names.length) {
                names.forEach(function (name) {
                    $(document).on('click', '[data-modal-target="' + name + '"]', function (e) {
                        e.preventDefault();
                        self.myModal('show', $(this).data('modalTarget'), $(this).data('modalTitle'));
                    });
                })
            }
            $(document).on('click', '.modal__close, .modal__overlay', function (e) {
                self.myModal('hide');
            });
            return this;
        },
        show: function (name) {
            this.myModal('hideAll');
            this.closest('.modal').addClass('modal_show');
            this.each(function () {
                if ($(this).data('modal') === name) {
                    $(this).addClass('modal__content_show');
                }
            });
        },
        hide: function () {
            var container = this.closest('.modal');
            container.removeClass('modal_show');
            container.find('.modal__content_show').removeClass('modal-content__show');
        },
        hideAll: function () {
            var container = this.closest('.modal');
            container.removeClass('modal_show');
            container.find('.modal__content_show').removeClass('modal__content_show');
        }
    };

    $.fn.myModal = function( method ) {
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метод с именем ' +  method + ' не существует для jQuery.mymodal' );
        }
    };
})( jQuery );