$(function () {
    $(document).on('click', '.js_mobile-menu-open', function () {
        console.log($(this).closest('.js_mobile-menu-container'))
        var container = $(this).closest('.header');
        $(this).toggleClass('burger-btn_active');
        container.find('.mobile-menu-wrapper').toggleClass('mobile-menu-wrapper_active');
        container.find('.mobile-menu').toggleClass('mobile-menu_active');
    });
    $('.js_main-slider').slick({
        arrows: false,
        dots: true
    });
    $('.js_objects-slider').slick({
        arrows: true,
        dots: false,
        slidesToShow: 4,
        appendArrows: $('.js_objects-slider-controls'),
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    $('.js_news-slider').slick({
        arrows: true,
        dots: false,
        slidesToShow: 4,
        appendArrows: $('.js_news-slider-controls'),
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    $('.modal__content').myModal();

    $('.js_checkbox').on('click', function () {
        $(this).toggleClass('checkbox__input_checked');
    });
});